FROM php:5.6-apache
COPY . /app/
# Setup the Composer installer
ENV COMPOSER_INSTALLER_SHA384 "92102166af5abdb03f49ce52a40591073a7b859a86e8ff13338cf7db58a19f7844fbc0bb79b2773bf30791e935dbd938"
RUN echo "<?php readfile('https://getcomposer.org/installer');" | php > /tmp/composer-setup.php \
  && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== '${COMPOSER_INSTALLER_SHA384}') { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }"

RUN php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer  && rm -rf /tmp/composer-setup.php

WORKDIR /app
RUN composer install

RUN rm -rf /var/www/html && ln -s /app/web /var/www/html
