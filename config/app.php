<?php

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use NgsDemo\Infrastructure\Repository\ApiVacanciesRepository;
use NgsDemo\VacanciesReport\Helper\WordsSplitterInterface;
use NgsDemo\VacanciesReport\Repository\VacanciesRepositoryInterface;
use NgsDemo\VacanciesReport\Service\VacanciesReportService;
use NgsDemo\VacanciesReport\Service\VacanciesReportServiceInterface;

$app = new Silex\Application();

$app['config'] = [
    'vacanciesEndpoint' => 'http://rabota.ngs.ru/api/v1/vacancies/',
    'geo' => [
        'Novosibirsk' => 826
    ]
];

$app[VacanciesReportServiceInterface::class] = function($app) {
    return new VacanciesReportService(
        $app[VacanciesRepositoryInterface::class],
        $app[WordsSplitterInterface::class]
    );
};

$app[WordsSplitterInterface::class] = function () {
    return new \NgsDemo\VacanciesReport\Helper\BaseWordsSplitter();
};

$app[VacanciesRepositoryInterface::class] = function ($app) {
    return new ApiVacanciesRepository(
        $app[ClientInterface::class],
        $app['config']['vacanciesEndpoint']
    );
};

$app[ClientInterface::class] = function () {
    return new Client();
};

$app->register(new Silex\Provider\TwigServiceProvider(), [
    'twig.path' => __DIR__.'/../src/web/views',
]);

$app['debug'] = true;

return $app;
