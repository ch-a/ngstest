<?php
/**
 * @author Alexey Cherkasov <cherkasov.alek@gmail.com>
 */

namespace NgsDemo\Infrastructure\Api;


class Metadata
{

    /**
     * @var ResultSet
     */
    private $resultSet;

    /**
     * @return ResultSet
     */
    public function getResultSet()
    {
        return $this->resultSet;
    }

    /**
     * @param ResultSet $resultSet
     */
    public function setResultSet($resultSet)
    {
        $this->resultSet = $resultSet;
    }
    
}
