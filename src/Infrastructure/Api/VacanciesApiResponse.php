<?php
/**
 * @author Alexey Cherkasov <cherkasov.alek@gmail.com>
 */

namespace NgsDemo\Infrastructure\Api;


use NgsDemo\VacanciesReport\Entity\Vacancy;

/**
 * Class VacanciesApiResponse
 * @package NgsDemo\Infrastructure\Api
 */
class VacanciesApiResponse
{
    /**
     * @var Metadata
     */
    private $metadata;

    /**
     * @var Vacancy[]
     */
    private $vacancies;

    /**
     * VacanciesApiResponse constructor.
     * @param Metadata $metadata
     * @param \NgsDemo\VacanciesReport\Entity\Vacancy[] $vacancies
     */
    public function __construct(Metadata $metadata, array $vacancies)
    {
        $this->metadata = $metadata;
        $this->vacancies = $vacancies;
    }


    /**
     * @return Metadata
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @return \NgsDemo\VacanciesReport\Entity\Vacancy[]
     */
    public function getVacancies()
    {
        return $this->vacancies;
    }
}
