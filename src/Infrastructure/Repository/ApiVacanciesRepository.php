<?php
/**
 * @author Alexey Cherkasov <cherkasov.alek@gmail.com>
 */

namespace NgsDemo\Infrastructure\Repository;

use GuzzleHttp\ClientInterface;
use NgsDemo\Infrastructure\Api\Metadata;
use NgsDemo\Infrastructure\Api\ResultSet;
use NgsDemo\Infrastructure\Api\VacanciesApiResponse;
use NgsDemo\VacanciesReport\Entity\Rubric;
use NgsDemo\VacanciesReport\Entity\Vacancy;
use NgsDemo\VacanciesReport\Repository\VacanciesRepositoryInterface;

/**
 * Class ApiVacanciesRepository
 * @package NgsDemo\Infrastructure\Repository
 */
class ApiVacanciesRepository implements VacanciesRepositoryInterface
{

    const MAX_REQUEST_LIMIT = 100; // Больше 100 API не принемает limit

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var string
     */
    private $resourceEndpoint;

    /**
     * @var Rubric[]
     */
    private $rubricsEntityList = [];

    /**
     * ApiVacanciesRepository constructor.
     * @param ClientInterface $httpClient
     * @param string $resourceEndpoint
     */
    public function __construct(ClientInterface $httpClient, $resourceEndpoint)
    {
        $this->httpClient = $httpClient;
        $this->resourceEndpoint = $resourceEndpoint;
    }


    /**
     * @param int $geoId
     *
     * @return \NgsDemo\VacanciesReport\Entity\Vacancy[]
     */
    public function getNewForToday($geoId)
    {
        $data = $this->sendNewForTodayRequest($geoId, self::MAX_REQUEST_LIMIT, 0);
        $step = self::MAX_REQUEST_LIMIT;
        $vacancies = $data->getVacancies();
        $meta = $data->getMetadata();
        //Way to improve: Переделать в отправку пачкой запросов
        for ($offset = $step; $offset < $meta->getResultSet()->getCount(); $offset += $step) {
            $nextChunk = $this->sendNewForTodayRequest($geoId, self::MAX_REQUEST_LIMIT, $offset);
            $vacancies = array_merge($vacancies, $nextChunk->getVacancies());
            $meta = $nextChunk->getMetadata();
        }

        return $vacancies;
    }

    /**
     * @param $geoId
     * @param $limit
     * @param $offset
     * @return VacanciesApiResponse
     */
    private function sendNewForTodayRequest($geoId, $limit, $offset)
    {
        $url = $this->getRequestUrl([
            'limit'       => $limit,
            'offset'      => $offset,
            'period'      => 'today',
            'is_new_only' => 1,
            'geo_id'      => $geoId
        ]);
        $response = $this->httpClient->request('GET', $url);
        $data = json_decode($response->getBody()->getContents(), true);

        $meta = $this->extractMetadata($data['metadata']);
        $vacancies = $this->extractVacancies($data['vacancies']);
        return new VacanciesApiResponse($meta, $vacancies);

    }

    /**
     * @param $options
     * @return string
     */
    private function getRequestUrl(array $options)
    {
        return $this->resourceEndpoint . '?' . http_build_query($options);
    }

    /**
     * @param $metadata
     *
     * @return Metadata
     */
    private function extractMetadata(array $metadata)
    {
        $meta = new Metadata();
        $resultSet = new ResultSet();
        $resultSet->setCount($metadata['resultset']['count']);
        $resultSet->setLimit($metadata['resultset']['limit']);
        $resultSet->setOffset($metadata['resultset']['offset']);
        $meta->setResultSet($resultSet);

        return $meta;
    }

    /**
     * @param array $vacancies
     *
     * @return Vacancy[]
     */
    private function extractVacancies(array $vacancies)
    {
        $result = [];
        foreach ($vacancies as $row) {
            $vacancy = new Vacancy();
            $vacancy->setId($row['id']);
            $vacancy->setHeader($row['header']);
            $rubrics = [];
            foreach ($row['rubrics'] as $rubricData) {
                if (!isset($this->rubricsEntityList[$rubricData['id']])) {
                    $rubric = new Rubric();
                    $rubric->setId($rubricData['id']);
                    $rubric->setTitle($rubricData['title']);
                    $this->rubricsEntityList[$rubricData['id']] = $rubric;
                }
                $rubrics[] = $this->rubricsEntityList[$rubricData['id']];
            }
            $vacancy->setRubrics($rubrics);
            $result[] = $vacancy;
        }

        return $result;
    }
}
