<?php
/**
 * @author Alexey Cherkasov <cherkasov.alek@gmail.com>
 */

namespace NgsDemo\VacanciesReport\Entity;

/**
 * Class Vacancy
 * @package NgsDemo\Infrastructure\Api
 */
class Vacancy
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $header;

    /**
     * @var Rubric[]
     */
    private $rubrics;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param string $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * @return Rubric[]
     */
    public function getRubrics()
    {
        return $this->rubrics;
    }

    /**
     * @param Rubric[] $rubrics
     */
    public function setRubrics(array $rubrics)
    {
        $this->rubrics = $rubrics;
    }
}
