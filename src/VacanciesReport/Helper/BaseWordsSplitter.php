<?php
/**
 * @author Alexey Cherkasov <cherkasov.alek@gmail.com>
 */

namespace NgsDemo\VacanciesReport\Helper;

/**
 * Class BaseWordsSplitter
 * Базовая имплементация. Без учета морфологии
 * @package NgsDemo\VacanciesReport\Helper
 */
class BaseWordsSplitter implements WordsSplitterInterface
{

    /**
     * @param $string
     * @return string[]
     */
    public function __invoke($string)
    {
        return array_filter(array_map(function ($w) {
            return trim($w, " \t,.()[]");
        }, explode(' ', $string)));
    }
}
