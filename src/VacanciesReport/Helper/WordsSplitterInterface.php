<?php
/**
 * @author Alexey Cherkasov <cherkasov.alek@gmail.com>
 */

namespace NgsDemo\VacanciesReport\Helper;

interface WordsSplitterInterface
{
    public function __invoke($string);
}
