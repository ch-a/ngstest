<?php

/**
 * @author Alexey Cherkasov <cherkasov.alek@gmail.com>
 */

namespace NgsDemo\VacanciesReport\Repository;

use NgsDemo\VacanciesReport\Entity\Vacancy;

interface VacanciesRepositoryInterface
{
    /**
     * @param $geoId
     * @return Vacancy[]
     */
    public function getNewForToday($geoId);
}
