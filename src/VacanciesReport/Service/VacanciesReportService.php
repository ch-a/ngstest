<?php
/**
 * @author Alexey Cherkasov <cherkasov.alek@gmail.com>
 */

namespace NgsDemo\VacanciesReport\Service;


use NgsDemo\VacanciesReport\Helper\WordsSplitterInterface;
use NgsDemo\VacanciesReport\Repository\VacanciesRepositoryInterface;

/**
 * Class RubricsReportService
 * @package NgsDemo\VacanciesReport\Service
 */
class VacanciesReportService implements VacanciesReportServiceInterface
{

    /**
     * @var VacanciesRepositoryInterface
     */
    private $vacanciesRepository;

    /**
     * @var WordsSplitterInterface
     */
    private $wordsSplitterHelper;

    /**
     * RubricsReportService constructor.
     * @param VacanciesRepositoryInterface $vacanciesRepository
     * @param WordsSplitterInterface $wordsSplitterHelper
     */
    public function __construct(
        VacanciesRepositoryInterface $vacanciesRepository,
        WordsSplitterInterface $wordsSplitterHelper
    ) {
        $this->vacanciesRepository = $vacanciesRepository;
        $this->wordsSplitterHelper = $wordsSplitterHelper;
    }

    /**
     * @param int $geoId
     * @return array
     */
    public function getTopRubricsByNewVacanciesToday($geoId)
    {
        $vacancies = $this->vacanciesRepository->getNewForToday($geoId);
        $rubricCounter = [];
        foreach ($vacancies as $v) {
            foreach ($v->getRubrics() as $rubric) {
                if (!isset($rubricCounter[$rubric->getId()])) {
                    $rubricCounter[$rubric->getId()] = [
                        'rubric' => $rubric,
                        'count'  => 0
                    ];
                }
                $rubricCounter[$rubric->getId()]['count']++;
            }
        }
        usort($rubricCounter, function ($a, $b) { return $b['count'] - $a['count']; });
        
        return $rubricCounter;
    }

    /**
     * @param int $geoId
     * @return array [word => count ....]
     */
    public function getTopWordInNewVacanciesTitleForToday($geoId)
    {
        $vacancies = $this->vacanciesRepository->getNewForToday($geoId);
        $top = [];
        $helper = $this->wordsSplitterHelper;
        foreach ($vacancies as $v) {
            foreach ($helper($v->getHeader()) as $word) {
                if (strlen($word) < 2) {
                    continue; // Отбрасываем часть предлогов
                }
                if (!isset($top[$word])) {
                    $top[$word] = [
                        'word' => $word,
                        'count' => 0
                    ];
                }
                $top[$word]['count']++;
            }
        }

        usort($top, function ($a, $b) { return $b['count'] - $a['count']; });


        return $top;
    }
}
