<?php
/**
 * @author Alexey Cherkasov <cherkasov.alek@gmail.com>
 */
namespace NgsDemo\VacanciesReport\Service;


/**
 * Class RubricsReportService
 * @package NgsDemo\VacanciesReport\Service
 */
interface VacanciesReportServiceInterface
{
    /**
     * @param int $geoId
     */
    public function getTopRubricsByNewVacanciesToday($geoId);

    /**
     * @param int $geoId
     */
    public function getTopWordInNewVacanciesTitleForToday($geoId);
}
