<?php

use NgsDemo\VacanciesReport\Service\VacanciesReportServiceInterface;

date_default_timezone_set('Asia/Novosibirsk');

require_once __DIR__ . '/../vendor/autoload.php';

$app = require __DIR__.'/../config/app.php';

$app->get('/', function() use ($app) {
    /** @var VacanciesReportServiceInterface $vacanciesReportService */
    $vacanciesReportService = $app[VacanciesReportServiceInterface::class];

    $geoId = $app['config']['geo']['Novosibirsk'];
    return $app['twig']->render('index.twig', array(
        'rubrics' => $vacanciesReportService->getTopRubricsByNewVacanciesToday($geoId),
        'words'   => $vacanciesReportService->getTopWordInNewVacanciesTitleForToday($geoId)
    ));
});

$app->run();
